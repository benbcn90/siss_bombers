package com.bombers.x7044508e.siss_bombers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bombers.x7044508e.siss_bombers.adapter.DataListAdapter;
import com.bombers.x7044508e.siss_bombers.controller.DataList;
import com.bombers.x7044508e.siss_bombers.controller.DataListAssignation;
import com.bombers.x7044508e.siss_bombers.parser.JsonParser;

import static com.bombers.x7044508e.siss_bombers.util.Configuration.LIST_DATA_URL;

public class MainActivity extends AppCompatActivity {

    Button assignFireman,viewData,showLocations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        assignFireman=(Button)findViewById(R.id.assignsFireman);
        viewData=(Button)findViewById(R.id.viewData);
        showLocations=(Button)findViewById(R.id.showLocations);

        sendRequest();

        assignFireman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(),DataListAssignation.class);
                startActivity(intent);

            }
        });

        viewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DataList.class);
                startActivity(intent);

            }
        });

        showLocations.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(intent);
            }
        });

    }

    private void sendRequest(){
        final ProgressDialog loading = ProgressDialog.show(this,"Actualizando...","Por favor espera...",false,false);

        StringRequest stringRequest = new StringRequest(LIST_DATA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);

                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showJSON(String json){
        JsonParser pj = new JsonParser(json);
        pj.parseJSON();
    }
}
