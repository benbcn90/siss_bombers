package com.bombers.x7044508e.siss_bombers.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.bombers.x7044508e.siss_bombers.converter.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Asignacion implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String nombreFurgonSalvamento;
    private String nombreLugarDesastre;
    private int nivelPrioridad;
    private Double distanciaEntrePuntos;
    @TypeConverters(DateConverter.class)
    private Date fechaAsignacion;

    public Asignacion() {
    }

    public Asignacion(String nombreFurgonSalvamento, String nombreLugarDesastre, int nivelPrioridad, Double distanciaEntrePuntos, Date fechaAsignacion) {
        this.nombreFurgonSalvamento = nombreFurgonSalvamento;
        this.nombreLugarDesastre = nombreLugarDesastre;
        this.nivelPrioridad = nivelPrioridad;
        this.distanciaEntrePuntos = distanciaEntrePuntos;
        this.fechaAsignacion = fechaAsignacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreFurgonSalvamento() {
        return nombreFurgonSalvamento;
    }

    public void setNombreFurgonSalvamento(String nombreFurgonSalvamento) {
        this.nombreFurgonSalvamento = nombreFurgonSalvamento;
    }

    public String getNombreLugarDesastre() {
        return nombreLugarDesastre;
    }

    public void setNombreLugarDesastre(String nombreLugarDesastre) {
        this.nombreLugarDesastre = nombreLugarDesastre;
    }

    public int getNivelPrioridad() {
        return nivelPrioridad;
    }

    public void setNivelPrioridad(int nivelPrioridad) {
        this.nivelPrioridad = nivelPrioridad;
    }

    public Double getDistanciaEntrePuntos() {
        return distanciaEntrePuntos;
    }

    public void setDistanciaEntrePuntos(Double distanciaEntrePuntos) {
        this.distanciaEntrePuntos = distanciaEntrePuntos;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    @Override
    public String toString() {
        return "Asignacion{" +
                "id=" + id +
                ", nombreFurgonSalvamento='" + nombreFurgonSalvamento + '\'' +
                ", nombreLugarDesastre='" + nombreLugarDesastre + '\'' +
                ", nivelPrioridad='" + nivelPrioridad + '\'' +
                ", distanciaEntrePuntos=" + distanciaEntrePuntos +
                ", fechaAsignacion=" + fechaAsignacion +
                '}';
    }
}
