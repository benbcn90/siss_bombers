package com.bombers.x7044508e.siss_bombers.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.bombers.x7044508e.siss_bombers.Dao.AsignacionDao;
import com.bombers.x7044508e.siss_bombers.entity.Asignacion;

@Database(entities = {Asignacion.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "bomberos_cat_db")
                            .build();
        }
        return INSTANCE;
    }

    public abstract AsignacionDao getAsignacionDao();

}
