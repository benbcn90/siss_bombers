package com.bombers.x7044508e.siss_bombers.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.bombers.x7044508e.siss_bombers.R;
import com.bombers.x7044508e.siss_bombers.adapter.DataListAdapter;
import com.bombers.x7044508e.siss_bombers.adapter.DataListAssignmentAdapter;
import com.bombers.x7044508e.siss_bombers.entity.Asignacion;
import com.bombers.x7044508e.siss_bombers.entity.Desastre;
import com.bombers.x7044508e.siss_bombers.entity.FurgonSalvamento;
import com.bombers.x7044508e.siss_bombers.parser.JsonParser;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DataListAssignation extends AppCompatActivity {
    private ArrayList<FurgonSalvamento> furgonSalvamentoList;
    private ArrayList<Desastre> desastreList;
    private ArrayList<Asignacion> asignacionList = new ArrayList<>();
    private ListView listAssignamentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_list_assignment);

        listAssignamentView = (ListView) findViewById(R.id.listAssignmentView);

        if(permitirAsignar()){
            DataListAssignmentAdapter assignmentListAdapter = new DataListAssignmentAdapter(this, asignacionList);
            listAssignamentView.setAdapter(assignmentListAdapter);
        }
    }

    private boolean permitirAsignar(){
        if(JsonParser.dNombre != null){
            rellenarListas();
            asignar();
            return true;
        }
        return false;
    }

    private void rellenarListas(){
        furgonSalvamentoList = new ArrayList<>();
        desastreList = new ArrayList<>();
        int posiciones = JsonParser.dNombre.length;

        for(int i = 0; i < posiciones; i++){
            if(JsonParser.dEsBombero[i].equals("TRUE")){
                furgonSalvamentoList.add(new FurgonSalvamento(JsonParser.dNombre[i], Double.parseDouble(JsonParser.dLatitud[i].replaceAll(",",".")), Double.parseDouble(JsonParser.dLongitud[i].replaceAll(",","."))));
            }else{
                desastreList.add(new Desastre(JsonParser.dNombre[i], Double.parseDouble(JsonParser.dLatitud[i].replaceAll(",",".")),Double.parseDouble(JsonParser.dLongitud[i].replaceAll(",",".")),Integer.parseInt(JsonParser.dNivelPrioridad[i])));
            }
        }
    }

    private void  asignar(){
        //En este metodo segun las listas de bombeors y desastres se realizará laasignacion
        ordenar(desastreList);
        compararYAsignar(desastreList, furgonSalvamentoList);
    }

    private void compararYAsignar(ArrayList<Desastre> desastreList, ArrayList<FurgonSalvamento> furgonSalvamentoList) {
        for(Desastre desastre : desastreList){
            asignarFurgonSalvamento(desastre, furgonSalvamentoList);
        }
    }

    //Medimos la distancia entre el listado de bomberos y el desastre pasado como parametro para obetner la distancia más corta.
    //Luego eliminamos el bombero de la lista para no asignarlo a otro desastre.
    private void asignarFurgonSalvamento(Desastre desastre, ArrayList<FurgonSalvamento> furgonSalvamentoList) {
        Double distanciaMasCorta = null;
        Asignacion asignacion = new Asignacion();
        FurgonSalvamento furgonSalvamentoEliminar = new FurgonSalvamento();

        for(FurgonSalvamento furgonSalvamento : furgonSalvamentoList){
            Double distancia = SphericalUtil.computeDistanceBetween(new LatLng(desastre.getLatitud(), desastre.getLongitud()), new LatLng(furgonSalvamento.getLatitud(),furgonSalvamento.getLongitud()));
//            distanciaMasCorta = distanciaMasCorta == null || distanciaMasCorta >= distancia ? distancia : distanciaMasCorta;

            if(distanciaMasCorta == null || distanciaMasCorta > distancia){
                distanciaMasCorta = distancia;
                furgonSalvamentoEliminar = furgonSalvamento;
                asignacion = new Asignacion(furgonSalvamento.getNombre(), desastre.getNombre(), desastre.getNivelPrioridad(), distanciaMasCorta, null);
            }
        }
        //Rellenamos el listado de asignaciones.
        asignacionList.add(asignacion);

        //Elimianmos elbombero asignado de la lista de bomberos disponibles.
        //Faltar probar, si no funciona probar con java.util.Iterator
        furgonSalvamentoList.remove(furgonSalvamentoEliminar);

        //Falta controlar el mostrar los bomberosque no han sido asignadosa ninguna inundación.
    }

    //Se ordena el listado de desastres, por orden de nivel de prioridad para recorrerlos en ese orden y asignarle un bombero
    private void ordenar(ArrayList<Desastre> desastreList){
//        desastreList.stream().sorted(Comparator.comparing(Desastre::getNivelPrioridad)).collect(Collector.toList());
        Collections.sort(desastreList, new Comparator<Desastre>(){
            public int compare(Desastre o1, Desastre o2){
                if(o1.getNivelPrioridad() == o2.getNivelPrioridad())
                    return 0;
                return o1.getNivelPrioridad() < o2.getNivelPrioridad() ? -1 : 1;
            }
        });
    }
}
