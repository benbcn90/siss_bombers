package com.bombers.x7044508e.siss_bombers.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bombers.x7044508e.siss_bombers.model.DataModel;

import java.util.List;

import com.bombers.x7044508e.siss_bombers.R;
import com.squareup.picasso.Picasso;

public class DataListAdapter extends ArrayAdapter<String> {

    private String[] dNombre;
    private String[] dLatitud;
    private String[] dLongitud;
    private String[] dEsBombero;
    private String[] dNivelPrioridad;
    private Activity context;

    public DataListAdapter(Activity context, String[] dNombre, String[] dLatitud, String[] dLongitud, String[] dEsBombero, String[] dNivelPrioridad) {
        super(context, R.layout.layout_row_view, dNombre);
        this.context = context;
        this.dNombre = dNombre;
        this.dLatitud = dLatitud;
        this.dLongitud = dLongitud;
        this.dNivelPrioridad = dNivelPrioridad;
        this.dEsBombero = dEsBombero;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_row_view, null, true);
        TextView textViewNombre = (TextView) listViewItem.findViewById(R.id.textViewNombre);
        TextView textViewLatitud = (TextView) listViewItem.findViewById(R.id.textViewLatitud);
        TextView textViewLongitud = (TextView) listViewItem.findViewById(R.id.textViewLongitud);
        TextView textViewNivelPrioridad = (TextView) listViewItem.findViewById(R.id.textViewNivelPrioridad);
        TextView textViewEsBombero = (TextView) listViewItem.findViewById(R.id.textViewEsBombero);
//        ImageView iv = (ImageView)listViewItem.findViewById(R.id.imageView3);


        textViewNombre.setText(dNombre[position]);
        textViewLatitud.setText("Latitud: " +  dLatitud[position]);
        textViewLongitud.setText("Longitud: " + dLongitud[position]);
        textViewNivelPrioridad.setText(new StringBuilder("Nivel de prioridad: ").append(dNivelPrioridad[0].equals("0") ? "Ninguno" : dNivelPrioridad[0]));
        textViewEsBombero.setText(new StringBuilder("¿Es Bombero?: ").append(dEsBombero[position].equals("0") ? "No" : "Si"));
        // Uri uri = Uri.parse(uImages[position]);
        //Uri uri = Uri.parse("https://drive.google.com/uc?id=0B___GhMLUVtOY09SbDU5cDU2T1U");
        // draweeView.setImageURI(uri);

//        Picasso.with(context).load(uImages[position]).into(iv);

        return listViewItem;
    }
}