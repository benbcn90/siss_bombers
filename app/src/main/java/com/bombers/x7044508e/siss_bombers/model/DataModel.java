package com.bombers.x7044508e.siss_bombers.model;

public class DataModel {
    private String nombre;
    private String ubicacionCoordenada;
    private String esBombero;
    private String nivelPrioridad;

    public DataModel() { }

    public DataModel(String nombre, String ubicacionCoordenada, String esBombero, String nivelPrioridad) {
        this.nombre = nombre;
        this.ubicacionCoordenada = ubicacionCoordenada;
        this.esBombero = esBombero;
        this.nivelPrioridad = nivelPrioridad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacionCoordenada() {
        return ubicacionCoordenada;
    }

    public void setUbicacionCoordenada(String ubicacionCoordenada) {
        this.ubicacionCoordenada = ubicacionCoordenada;
    }

    public String getEsBombero() {
        return esBombero;
    }

    public void setEsBombero(String esBombero) {
        this.esBombero = esBombero;
    }

    public String getNivelPrioridad() {
        return nivelPrioridad;
    }

    public void setNivelPrioridad(String nivelPrioridad) {
        this.nivelPrioridad = nivelPrioridad;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "name='" + nombre + '\'' +
                ", ubicacionCoordenada='" + ubicacionCoordenada + '\'' +
                ", esBombero='" + esBombero + '\'' +
                ", nivelPrioridad='" + nivelPrioridad + '\'' +
                '}';
    }
}
