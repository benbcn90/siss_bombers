package com.bombers.x7044508e.siss_bombers.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.bombers.x7044508e.siss_bombers.entity.Asignacion;

import java.util.List;

@Dao
public interface AsignacionDao {
    @Query("select * from asignacion")
    LiveData<List<Asignacion>> getAsignados();

    @Insert
    void addAsignacion(Asignacion asignacion);

    @Insert
    void addAsignaciones(List<Asignacion> asignacion);

    @Delete
    void deleteAsignacion(Asignacion asignacion);

    @Query("DELETE FROM asignacion")
    void deleteAsignaciones();

}
