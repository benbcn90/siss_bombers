package com.bombers.x7044508e.siss_bombers.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.bombers.x7044508e.siss_bombers.util.Configuration.KEY_LATITUD;
import static com.bombers.x7044508e.siss_bombers.util.Configuration.KEY_LONGITUD;
import static com.bombers.x7044508e.siss_bombers.util.Configuration.KEY_NOMBRE;
import static com.bombers.x7044508e.siss_bombers.util.Configuration.KEY_NIVEL_PRIORIDAD;
import static com.bombers.x7044508e.siss_bombers.util.Configuration.KEY_ES_BOMBERO;

public class JsonParser {
    public static String[] dNombre;
    public static String[] dLatitud;
    public static String[] dLongitud;
    public static String[] dNivelPrioridad;
    public static String[] dEsBombero;


    private JSONArray data = null;

    private String json;

    public JsonParser(){}

    public JsonParser(String json){
        this.json = json;
    }

    public void parseJSON(){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            data = jsonObject.getJSONObject("feed").getJSONArray("entry");
            Log.d("DEBUG DATA", data.toString());

            dNombre = new String[data.length()];
            dLatitud = new String[data.length()];
            dLongitud = new String[data.length()];
            dNivelPrioridad = new String[data.length()];
            dEsBombero = new String[data.length()];

            for(int i=0;i<data.length();i++){
                JSONObject jo = data.getJSONObject(i);
                dNombre[i] = jo.getJSONObject(KEY_NOMBRE).getString("$t");
                dLatitud[i] = jo.getJSONObject(KEY_LATITUD).getString("$t");
                dLongitud[i] = jo.getJSONObject(KEY_LONGITUD).getString("$t");
                dEsBombero[i] = jo.getJSONObject(KEY_ES_BOMBERO).getString("$t");
                dNivelPrioridad[i] = jo.getJSONObject(KEY_NIVEL_PRIORIDAD).getString("$t");
            }
            // Log.e("uImage","ser image"+uImages[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
