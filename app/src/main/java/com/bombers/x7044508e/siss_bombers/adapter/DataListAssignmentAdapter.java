package com.bombers.x7044508e.siss_bombers.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bombers.x7044508e.siss_bombers.R;
import com.bombers.x7044508e.siss_bombers.entity.Asignacion;

import java.util.ArrayList;

public class DataListAssignmentAdapter extends ArrayAdapter<String> {
    private ArrayList<Asignacion> asignacionList;
    private Activity context;


    public DataListAssignmentAdapter(Activity context, ArrayList<Asignacion> asignacionList) {
        super(context, R.layout.layout_row_assignment_view, asignacionList.size());
        this.context = context;
        this.asignacionList = asignacionList;
    }

    @Override
    public int getCount() {
        return asignacionList.size();
    }

    @Override
    public long getItemId(int position) {
        return asignacionList.get(position).toString().length();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_row_assignment_view, null, true);
        TextView tvAssignNombreBombero = (TextView) listViewItem.findViewById(R.id.tvAssignNombreBombero);
        TextView tvDestination = (TextView) listViewItem.findViewById(R.id.tvDestination);
        TextView tvPriorityLevel = (TextView) listViewItem.findViewById(R.id.tvPriorityLevel);

        tvAssignNombreBombero.setText(asignacionList.get(position).getNombreFurgonSalvamento());
        tvDestination.setText(asignacionList.get(position).getNombreLugarDesastre());
        tvPriorityLevel.setText(Integer.toString(asignacionList.get(position).getNivelPrioridad()));

        return listViewItem;
    }
}
