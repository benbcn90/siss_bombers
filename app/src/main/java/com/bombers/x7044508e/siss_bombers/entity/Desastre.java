package com.bombers.x7044508e.siss_bombers.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Desastre implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String nombre;
    private Double latitud;
    private Double longitud;
    private int nivelPrioridad;

    public Desastre() {
    }

    public Desastre(String nombre, Double latitud, Double longitud, int nivelPrioridad) {
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
        this.nivelPrioridad = nivelPrioridad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public int getNivelPrioridad() {
        return nivelPrioridad;
    }

    public void setNivelPrioridad(int nivelPrioridad) {
        this.nivelPrioridad = nivelPrioridad;
    }

    @Override
    public String toString() {
        return "Desastre{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                ", nivelPrioridad='" + nivelPrioridad + '\'' +
                '}';
    }

}
