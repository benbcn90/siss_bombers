package com.bombers.x7044508e.siss_bombers;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.bombers.x7044508e.siss_bombers.parser.JsonParser;
import com.bombers.x7044508e.siss_bombers.util.FormatCoordinatesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
//import com.koushikdutta.ion.Ion;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Polyline;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        JsonParser j = new JsonParser();
        boolean showLocatios = j.dNombre != null;
        FormatCoordinatesUtil format = new FormatCoordinatesUtil();
        ArrayList<LatLng> points = new ArrayList<LatLng>();
        PolylineOptions lineOptions = new PolylineOptions();

        /*
            Test OSMDroid bonus pack
         */
/*
        //First, get a road manager
        RoadManager roadManager = new OSRMRoadManager(this);

        //Set-up your start and end points:
        ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>();
        GeoPoint startPoint = new GeoPoint(41.426677, 2.148014);
        waypoints.add(startPoint);
        GeoPoint endPoint = new GeoPoint(41.398523, 2.203274);
        waypoints.add(endPoint);

        //And retreive the road between those points
        Road road = roadManager.getRoad(waypoints);

        //then, build a Polyline with the route shape:
        Polyline roadOverlay = RoadManager.buildRoadOverlay(road);

        //Add this Polyline to the overlayts of your map
*/

        LatLng iesPoblenou = new LatLng(41.398523, 2.203274);
        LatLng parqueBomberoVallHebron = new LatLng(41.426677, 2.148014);
        //Añado todas las posiciones
/*        points.add(iesPoblenou);
        points.add(parqueBomberoVallHebron);
        // Agregamos todos los puntos en la ruta al objeto LineOptions
        lineOptions.addAll(points);
        //Definimos el grosor de las Polilíneas
        lineOptions.width(2);
        //Definimos el color de la Polilíneas
        lineOptions.color(Color.BLUE);

        // Dibujamos las Polilineas en el Google Map para cada ruta
        mMap.addPolyline(lineOptions);*/


//        Ion.with(this).load("https://maps.googleapis.com/maps/api/directions/json?origin=41.398523,2.203274&destination=41.426677,2.148014&key=AIzaSyALy_EwO9mNIpR9T8M-rIW8nVNef8Gv12U")
//                .asJsonObject()
//                .setCallback((e, jsonObject) -> {
//                    Log.e("XXXX", String.valueOf(jsonObject));
//                });


        if (showLocatios) {
            int spreadsheetFilas = j.dNombre.length;
            //Terminar de controlar el zoom inicial
            for (int i = 0; i < spreadsheetFilas; i++) {
                String latitud = j.dLatitud[i].replace(",",".");
                String longitud = j.dLongitud[i].replace(",",".");
                LatLng latLng = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));

                //Asigna el icono según si laubicación es un bombero o unainundación
                if(j.dEsBombero[i].equals("TRUE")){
                    mMap.addMarker(new MarkerOptions().position(latLng).title(j.dNombre[i]).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marcador_fireman)));
                }else{
                    mMap.addMarker(new MarkerOptions().position(latLng).title(j.dNombre[i]).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marcador_flood_02)));
                }
            }
//            LatLng iesPoblenou = new LatLng(41.398523, 2.203274);
//            LatLng parqueBomberoVallHebron = new LatLng(41.426677, 2.148014);
            mMap.addMarker(new MarkerOptions().position(iesPoblenou).title("IES Poblenou"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(iesPoblenou, 15));
        }
    }
}