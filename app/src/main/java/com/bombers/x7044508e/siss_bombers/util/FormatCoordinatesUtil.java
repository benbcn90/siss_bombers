package com.bombers.x7044508e.siss_bombers.util;

import java.util.LinkedHashSet;
import java.util.Set;

public class FormatCoordinatesUtil {

    public String removeDuplicate(String cadena) {
        char[] chars = cadena.toCharArray();
        Set<Character> charSet = new LinkedHashSet<Character>();
        for (char c : chars) {
            charSet.add(c);
        }

        StringBuilder sb = new StringBuilder();
        for (Character character : charSet) {
            sb.append(character);
        }
    return sb.toString();
    }

}
