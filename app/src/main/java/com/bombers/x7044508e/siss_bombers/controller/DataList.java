package com.bombers.x7044508e.siss_bombers.controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.bombers.x7044508e.siss_bombers.R;
import com.bombers.x7044508e.siss_bombers.adapter.DataListAdapter;
import com.bombers.x7044508e.siss_bombers.parser.JsonParser;

public class DataList extends AppCompatActivity {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_list);

        listView = (ListView) findViewById(R.id.listView);

        if(JsonParser.dNombre != null){
            DataListAdapter userListAdapter = new DataListAdapter(this, JsonParser.dNombre,JsonParser.dLatitud, JsonParser.dLongitud, JsonParser.dEsBombero, JsonParser.dNivelPrioridad);
            listView.setAdapter(userListAdapter);
        }
    }

}
